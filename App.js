import React, { useRef } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  View,
  Text,
  Image,
  StatusBar,
  TouchableOpacity,
} from 'react-native';
import ViewShot from 'react-native-view-shot';
import RNFetch from 'react-native-fs'
import nativeModule from './nativeModule';

const App: () => React$Node = () => {
  const viewShot = useRef('')

  const uploadToIg = () => {
    viewShot.current.capture()
      .then((data) => {
        const dirs = RNFetch.DocumentDirectoryPath;
        const path = dirs + `/image${Math.ceil(Math.random() * 200000000000)}.png`;
        RNFetch.writeFile(path, data, 'base64')
        .then((success) => {
          nativeModule.goToInstagram(path)
          console.log('FILE WRITTEN!', path);
        })
        .catch((err) => {
          console.log(err.message);
        });
      })
  }
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <View style={styles.container}>
        <ViewShot style={styles.content} ref={viewShot} options={{ format: "jpg", quality: 0.9, result: 'base64' }}>
          <Image
            source={{ uri: 'https://via.placeholder.com/300x300?text=No%20Image' }}
            style={styles.image}
          />
        </ViewShot>

        <TouchableOpacity style={styles.button} onPress={uploadToIg}>
          <Text style={styles.buttonText}>Upload to IG</Text>
        </TouchableOpacity>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f4f4f4',
    alignItems: 'center',
    justifyContent: 'center'
  },
  content: {
    width: 300,
    height: 300,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 20,
    backgroundColor: '#ffffff',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,

    elevation: 6,
  },
  image: {
    width: '100%',
    height: 300,
    resizeMode: 'contain'
  },
  button: {
    marginHorizontal: 30,
    marginVertical: 20,
    padding: 10,
    minWidth: 150,
    backgroundColor: '#fc039d',
    minHeight: 50,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,

    elevation: 6,
  },
  buttonText: {
    color: '#ffffff'
  }
});

export default App;
