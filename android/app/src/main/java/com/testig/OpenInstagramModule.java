package com.testig;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import java.io.File;

import androidx.core.content.FileProvider;

public class OpenInstagramModule extends ReactContextBaseJavaModule {
  private static ReactApplicationContext reactContext;

  private static final String DURATION_SHORT_KEY = "SHORT";
  private static final String DURATION_LONG_KEY = "LONG";

  OpenInstagramModule(ReactApplicationContext context) {
    super(context);
    reactContext = context;
  }

  @Override
  public String getName() {
    return "OpenInstagramModule";
  }

  @ReactMethod
  public void goToInstagram(String url) {
    Uri stickerAssetUri = FileProvider.getUriForFile(getReactApplicationContext(), BuildConfig.APPLICATION_ID + ".provider", new File(url));

    Log.d("stickerAsset", stickerAssetUri.toString());
    Log.d("stickerAsset", url);
    String sourceApplication = "com.testig";

    // Instantiate implicit intent with ADD_TO_STORY action,
    // background asset, and attribution link
    Intent intent = new Intent("com.instagram.share.ADD_TO_STORY");
    intent.putExtra("source_application", sourceApplication);

    intent.setType("image/*");
    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
    intent.putExtra("interactive_asset_uri", stickerAssetUri);
    intent.putExtra("top_background_color", "#33FF33");
    intent.putExtra("bottom_background_color", "#FF00FF");

    Activity activity = getCurrentActivity();
    activity.grantUriPermission(
        "com.instagram.android", stickerAssetUri, Intent.FLAG_GRANT_READ_URI_PERMISSION);
    if (activity.getPackageManager().resolveActivity(intent, 0) != null) {
      activity.startActivityForResult(intent, 0);
    }
  }

  @ReactMethod
  public void show(String message) {
    Toast.makeText(getReactApplicationContext(), message, Toast.LENGTH_SHORT).show();
  }
}
